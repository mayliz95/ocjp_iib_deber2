/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocjp_iib_deber2;

import javax.swing.JOptionPane;

/**
 *
 * @author Mayra
 */
public class Palindromo {
    
    String frase;

    public Palindromo() {
    }

    public Palindromo(String frase) {
        this.frase = frase;
    }

    public String getFrase() {
        return frase;
    }

    public void setFrase() {
        frase = JOptionPane.showInputDialog("Ingrese la frase a analizar");
    }
    
    public void esPalabraPalindromo() {
        
        String aux ="";
        int numL = frase.length();
        
        for (int i=0; i<numL; i++) {            
            aux += frase.substring(numL-(i+1), numL-i);
        }
                
        if (aux.equalsIgnoreCase(frase)) {
            System.out.println("La palabra: " + frase + " es un palindromo");        
        }
    }
    
    public void esFrasePalindromo() {
        
        String aux = frase.replace(" ", "").trim();
        String aux1 = "";
        int numL = aux.length();
        
        for (int i=0; i<numL; i++) {            
            aux1 += aux.substring(numL-(i+1), numL-i);
        }
                              
        if (aux1.equalsIgnoreCase(aux)) {
            System.out.println("La frase: \"" + frase + "\" es un palindromo");        
        }

    }
    
}
